using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotationScript : MonoBehaviour
{
    [SerializeField]
    public float RotationSpeed1;
    public Vector3 RotationAxis1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(RotationAxis1 * (RotationSpeed1 * Time.deltaTime));  
        
    }
}
