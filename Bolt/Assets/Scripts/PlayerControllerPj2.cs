﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerPj2 : MonoBehaviour {

    private Rigidbody rigidBody;        // Player's rigidbody
    private Animator animator;          // Player's animator
    private float hInput;               // horizontal input
    private float vInput;  
    public bool jump; 
    private float runInput1; 
    private bool runInput;             // Run buttom
    private float currentSpeed = 0;     // Current speed of the player
    public float walkSpeed = 3;         // Player speed when walking
    public float runSpeed = 6;          // Player speed when running 
    public float turnSmoothing = 20f;   // Turn speed
    public float Gravity = -9.8f;
    public float DashFactor = 2f;
    public float idleWaitingTime = 5f;  // Time to wait to try waiting animation in idle animator state
    private float currentWaitingTime = 0;   // Current time that player already waits
    public bool isGrounded;
    private Vector3 moveDirection;
    private Vector3 smoothMoveDiection;
    public Vector3 Drag = new Vector3(1f, 2f, 1f);
    private Vector3 smoother;
    public float speed = 5f;
    public float smoothTime = 0.15f;
    public float jumpHeight = 2f;
    public float forwardJumpFactor = 0.05f;
    private Vector3 horizontalVelocity;
    public float m_Thrust = 0.01f;
    bool jump1;
    // Use this for initialization
    void Start() {
        jump1=false;
        rigidBody = GetComponent<Rigidbody>();
        isGrounded=true;
        if (rigidBody == null) {
            Debug.Log("No rigidbody component found in this gameobject");
            enabled = false;
            return;
        }

        animator = GetComponent<Animator>();

        if (animator == null) {
            Debug.Log("No animator component found in this gameobject");
            enabled = false;
            return;
        }
	}
	
	// Update is called once per frame
	void Update () {

        // Input
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        runInput1 = Input.GetAxis("Dash");
        runInput = Input.GetButton("Dash");
        jump = Input.GetButtonDown("Jump");

        handleRotation();

        // Increase waiting time only when player speed is zero.
        if (currentSpeed == 0) {
            currentWaitingTime += Time.deltaTime;
        }
        else {
            currentWaitingTime = 0;
        }

        if(jump && isGrounded == true){
                jump1=true;
                rigidBody.AddForce(0, m_Thrust, 0, ForceMode.Impulse);
                isGrounded=false;
            
        
        }

    }

    private void OnAnimatorMove() {
        animator.SetFloat("Speed", currentSpeed, currentSpeed * Time.deltaTime, Time.deltaTime);
        if(jump1 == true){
            animator.SetTrigger("Jump");
            jump1=false;
        }
        if (currentWaitingTime >= idleWaitingTime) {
            if (Random.Range(0, 99) < 30) {
                animator.SetTrigger("IdleWait");
            }
            

            currentWaitingTime = 0;
        }
        
    }


    private void FixedUpdate() {
        handleMovement();
    }
    void OnCollisionEnter(Collision other){
        if (other.gameObject.CompareTag("Ground")){
            isGrounded = true;
        }
    }

    private void handleMovement() {
        // Walkspeed
        float targetSpeed = walkSpeed;

        if (runInput1 > 0)
            targetSpeed = runSpeed;

        // If we got input from horizontal or vertical axis
        if (hInput != 0 || vInput != 0) {
            rigidBody.velocity = new Vector3(hInput, 0, vInput) * targetSpeed;
        }
        else {
            rigidBody.velocity = Vector3.zero; // No input, stop the player
        }


        currentSpeed = rigidBody.velocity.magnitude; // velocity to speed conversion 


    }

    private void handleRotation() {
        // Rotation
        if (hInput != 0 || vInput != 0) {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(hInput, 0, vInput));
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * turnSmoothing); // How fast the player turns from one direction to another
        }
    }
}

 